#include "mainwindow.h"
#include "ui_mainwindow.h"

#define SQUARE_RANGE 10

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    prepareGraphs();
    shouldContinue = false;
    currentEpoch = 1;
    QTimer* timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &MainWindow::nextEpoch);
    timer->start(100);
    ui->epochs->setValue(25);
    ui->learningRate->setValue(QRandomGenerator::global()->bounded(1));
    canStart = false;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::prepareGraphs()
{
    connect(ui->mainPlot, &QCustomPlot::mouseDoubleClick, this, &MainWindow::onDoubleClick);
    ui->mainPlot->addGraph();
    ui->mainPlot->addGraph();
    ui->mainPlot->addGraph();
    ui->mainPlot->addGraph();
    ui->mainPlot->plotLayout()->insertRow(0);
    ui->mainPlot->plotLayout()->addElement(0, 0, new QCPTextElement(ui->mainPlot, "Main Grid", QFont("sans", 12, QFont::Medium)));
    ui->mainPlot->xAxis->setLabel("X");
    ui->mainPlot->yAxis->setLabel("Y");
    ui->mainPlot->xAxis->setRange(-SQUARE_RANGE, SQUARE_RANGE);
    ui->mainPlot->yAxis->setRange(-SQUARE_RANGE, SQUARE_RANGE);
    ui->mainPlot->graph(1)->setLineStyle(QCPGraph::lsNone);
    ui->mainPlot->graph(1)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, Qt::blue, 5));
    ui->mainPlot->graph(2)->setLineStyle(QCPGraph::lsNone);
    ui->mainPlot->graph(2)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssPlus, Qt::red, 5));
    ui->errorPlot->addGraph();
    ui->errorPlot->plotLayout()->insertRow(0);
    ui->errorPlot->plotLayout()->addElement(0, 0, new QCPTextElement(ui->errorPlot, "Errors by Epoch", QFont("sans", 12, QFont::Medium)));
    ui->errorPlot->xAxis->setLabel("Epoch");
    ui->errorPlot->yAxis->setLabel("Errors");
}

void MainWindow::onDoubleClick(QMouseEvent* e)
{
    QPointF point = QPointF(ui->mainPlot->xAxis->pixelToCoord(e->x()), ui->mainPlot->yAxis->pixelToCoord(e->y()));
    int graphNumber;
    switch (e->button()) {
        case Qt::LeftButton:
            graphNumber = 1;
            break;
        case Qt::RightButton:
            graphNumber = 2;
            break;
        default:
            graphNumber = 1;
            break;
    }
    QVector<double> data{point.x(), point.y()};
    trainingSet.append(QPair<QVector<double>, int>(data, graphNumber - 1));
    ui->mainPlot->graph(graphNumber)->addData(point.x(), point.y());
    ui->mainPlot->replot();
}

void MainWindow::on_clearButton_clicked()
{
    shouldContinue = false;
    canStart = false;
    trainingSet.clear();
    ui->epochs->setValue(100);
    ui->learningRate->setValue(QRandomGenerator::global()->bounded(1));
    ui->currentEpoch->setText("1");
    ui->w0->setText("0");
    ui->w1->setText("0");
    ui->w2->setText("0");
    ui->mainPlot->graph(0)->setData(QVector<double>(), QVector<double>());
    ui->mainPlot->graph(1)->setData(QVector<double>(), QVector<double>());
    ui->mainPlot->graph(2)->setData(QVector<double>(), QVector<double>());
    ui->mainPlot->graph(3)->setData(QVector<double>(), QVector<double>());
    ui->mainPlot->replot();
    ui->errorPlot->graph(0)->setData(QVector<double>(), QVector<double>());
    ui->errorPlot->replot();
    currentEpoch = 1;
}

void MainWindow::carpetPlot()
{
    for (double x = -SQUARE_RANGE; x < SQUARE_RANGE; x += .1)
    {
        for (double y = -SQUARE_RANGE; y < SQUARE_RANGE; y += .1)
        {
            QVector<double> data{x, y};
            ui->mainPlot->graph(perceptron.evaluate(data)+1)->addData(x, y);
        }
    }
    ui->mainPlot->replot();
}

void MainWindow::on_startButton_clicked()
{
    if (!canStart) {
        return;
    }
    shouldContinue = true;
}

void MainWindow::nextEpoch()
{
    if (shouldContinue)
    {
        auto errors = perceptron.learn(trainingSet);
        ui->errorPlot->xAxis->setRange(0, currentEpoch);
        ui->errorPlot->graph(0)->addData(currentEpoch, errors);
        ui->errorPlot->replot();
        currentEpoch++;
        ui->currentEpoch->setText(QString::number(currentEpoch));

        auto a = perceptron.weights[0];
        auto b = perceptron.weights[1];
        auto c = perceptron.weights[2];
        ui->w0->setText(QString::number(a));
        ui->w1->setText(QString::number(b));
        ui->w2->setText(QString::number(c));
        ui->mainPlot->graph(0)->setData(QVector<double>(), QVector<double>());
        ui->mainPlot->graph(0)->addData(-10, -((a * -10) - c) / b);
        ui->mainPlot->graph(0)->addData(10, -((a * 10) - c) / b);
        ui->mainPlot->replot();

        if (!errors or currentEpoch == maxEpochs)
        {
            if (!errors)
            {
                QMessageBox(QMessageBox::Information, "Finished", "Found correct weights").exec();
            }
            else
            {
                QMessageBox(QMessageBox::Information, "Finished", "Max epochs reached").exec();
            }
            shouldContinue = false;
            carpetPlot();
        }
    }
}

void MainWindow::on_initButton_clicked()
{
    currentEpoch = 0;
    double learningRate = ui->learningRate->value();
    maxEpochs = ui->epochs->value();
    perceptron = Perceptron(2, learningRate);
    ui->errorPlot->xAxis->setRange(0, 1);
    ui->errorPlot->yAxis->setRange(0, trainingSet.size());
    canStart = true;
    auto a = perceptron.weights[0];
    auto b = perceptron.weights[1];
    auto c = perceptron.weights[2];
    ui->w0->setText(QString::number(a));
    ui->w1->setText(QString::number(b));
    ui->w2->setText(QString::number(c));
    ui->mainPlot->graph(3)->setData(QVector<double>(), QVector<double>());
    ui->mainPlot->graph(3)->addData(-10, -((a * -10) - c) / b);
    ui->mainPlot->graph(3)->addData(10, -((a * 10) - c) / b);
    ui->mainPlot->replot();
}
