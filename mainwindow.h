#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <QPair>
#include <QVector>

#include "perceptron.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_clearButton_clicked();

    void on_startButton_clicked();

    void nextEpoch();

    void on_initButton_clicked();

private:
    Ui::MainWindow *ui;
    QVector<QPair<QVector<double>, int>> trainingSet;
    Perceptron perceptron;
    int maxEpochs;
    int currentEpoch;
    bool shouldContinue;
    bool canStart;

    void prepareGraphs();
    void onDoubleClick(QMouseEvent* e);
    void carpetPlot();
};

#endif // MAINWINDOW_H
