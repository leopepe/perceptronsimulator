#ifndef PERCEPTRON_H
#define PERCEPTRON_H

#include <QVector>
#include <QPair>
#include <QRandomGenerator>

class Perceptron
{
public:
    Perceptron();
    Perceptron(int, double);
    void init(int, double);
    int evaluate(QVector<double>);
    int learn(QVector<QPair<QVector<double>, int>>);
    QVector<double> weights;
private:
    double learningRate;
};

#endif // PERCEPTRON_H
