#include "perceptron.h"

Perceptron::Perceptron()
{

}

Perceptron::Perceptron(int inputsNumber, double learningRate)
{
    init(inputsNumber, learningRate);
}

void Perceptron::init(int inputsNumber, double learningRate)
{
    this->learningRate = learningRate;
    weights.clear();
    for (auto i = 0; i < inputsNumber+1; i++)
    {
        weights.append(QRandomGenerator::global()->bounded(10) * 2 - 10);
    }
}

int Perceptron::evaluate(QVector<double> inputs)
{
    double sum = 0;
    inputs.append(-1);
    for (auto i = 0; i < weights.size(); i++)
    {
        sum += weights[i] * inputs[i];
    }
    return sum > 0;
}

int Perceptron::learn(QVector<QPair<QVector<double>, int>> data)
{
    int errors = 0;
    for (auto testCase : data)
    {
        testCase.first.append(-1);
        auto error = testCase.second - evaluate(testCase.first);
        if (error)
        {
            errors++;
            for (auto i = 0; i < weights.size(); i++)
            {
                weights[i] += learningRate * error * testCase.first[i];
            }
        }
    }
    return errors;
}
